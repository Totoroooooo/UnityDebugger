namespace TT.Debugger { 
    using UnityEngine;
    using System;

    public enum TagType { And, Or }

    public static class Debugger {

        private const string PREFIX = "<b><color=#{1}>{0}</color></b>";
        private const string FOCUS_START_KEY = "<f>";
        private const string FOCUS_END_KEY = "</f>";
        private const string FOCUS_END_COLOR = "</b></color>";

        private static string FOCUS_START_COLOR => $"<color=#{ColorUtility.ToHtmlStringRGB(Settings.FocusColor.Get())}><b>";

        private static DebuggerSettings Settings {
            get {
                if (!Application.isPlaying) {
                    _settings = null;
                }
                if (_settings == null) {
                    _settings = DebuggerSettings.Get;
                }
                return _settings;
            }
        }
        private static DebuggerSettings _settings;

        private static string Prefix(this object logger) => string.Format(PREFIX, logger, ColorUtility.ToHtmlStringRGB(Settings.PrefixColor(logger.GetType())));

        private static void BaseLog(this object logger, object message, Action<object> logMethod, string[] tags, TagType type = TagType.And) {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            if (type == TagType.And) {
                foreach (var tag in tags) {
                    if (Settings.Tags.ContainsKey(tag) && !Settings.Tags[tag].Get()) {
                        return;
                    }
                }

            } else {
                bool noTag = true;
                bool validTag = false;
                foreach (var tag in tags) {
                    if (Settings.Tags.ContainsKey(tag)) {
                        noTag = false;
                        if (Settings.Tags[tag].Get()) {
                            validTag = true;
                            break;
                        }
                    }
                }
                if (!(noTag || validTag)) {
                    return;
                }
            }

            logMethod?.Invoke($"{logger.Prefix()}\n{message.ToString().Replace(FOCUS_START_KEY, FOCUS_START_COLOR).Replace(FOCUS_END_KEY, FOCUS_END_COLOR)}");
#endif
        }

        /// <summary>
        /// Log a message if all its tags are enabled. A tag not on the list is by default considered enabled
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="tags"></param>
        public static void Log(this object logger, object message, params string[] tags) => BaseLog(logger, message, Debug.Log, tags);
        /// <summary>
        /// Log a message if its tags are valid. If type is TagType.And, its tags are considered valid if they are all enabled. If type is TagType.Or, its tags are considered valid if at least one is enabled. A tag not on the list doesn't affect this outcome.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="tags"></param>
        public static void Log(this object logger, object message, TagType type, params string[] tags) => BaseLog(logger, message, Debug.Log, tags, type);

        /// <summary>
        /// Log a message if all its tags are enabled. A tag not on the list is by default considered enabled
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="tags"></param>
        public static void LogError(this object logger, object message, params string[] tags) => BaseLog(logger, message, Debug.LogError, tags);
        /// <summary>
        /// Log a message if its tags are valid. If type is TagType.And, its tags are considered valid if they are all enabled. If type is TagType.Or, its tags are considered valid if at least one is enabled. A tag not on the list doesn't affect this outcome.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="tags"></param>
        public static void LogError(this object logger, object message, TagType type, params string[] tags) => BaseLog(logger, message, Debug.LogError, tags,type);

        /// <summary>
        /// Log a  message if all its tags are enabled. A tag not on the list is by default considered enabled
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="tags"></param>
        public static void LogWarning(this object logger, object message, params string[] tags) => BaseLog(logger, message, Debug.LogWarning, tags);
        /// <summary>
        /// Log a message if its tags are valid. If type is TagType.And, its tags are considered valid if they are all enabled. If type is TagType.Or, its tags are considered valid if at least one is enabled. A tag not on the list doesn't affect this outcome.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="tags"></param>
        public static void LogWarning(this object logger, object message, TagType type, params string[] tags) => BaseLog(logger, message, Debug.LogWarning, tags, type);
    }
}
